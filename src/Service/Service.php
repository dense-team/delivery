<?php
/**
 * User: Maros Jasan
 * Date: 24. 2. 2021
 * Time: 17:52
 */

namespace Dense\Delivery\Service;

trait Service
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected \GuzzleHttp\Client $guzzle;

    /**
     * @var string
     */
    protected string $input = 'form';

    /**
     * @var string
     */
    protected string $output = 'json';

    /**
     * @var array
     */
    protected array $defaultParams = [];

    /**
     * @param array $defaultParams
     * @return $this
     */
    public function setDefaultParams(array $defaultParams): self
    {
        $this->defaultParams = $defaultParams;

        return $this;
    }

    /**
     * @param array $defaultParams
     * @return $this
     */
    public function mergeDefaultParams(array $defaultParams): self
    {
        $this->defaultParams = array_merge($this->defaultParams, $defaultParams);

        return $this;
    }

    /**
     * @return array
     */
    public function getDefaultParams(): array
    {
        return $this->defaultParams;
    }

    /**
     * @param array $params
     * @return array
     */
    protected function buildParams(array $params = []): array
    {
        return array_merge($this->getDefaultParams(), $params);
    }

    /**
     * @param array $query
     * @return array
     */
    protected function buildQuery(array $query = []): array
    {
        return $this->buildParams([
            'query' => $query,
        ]);
    }

    /**
     * @param array $input
     * @return array
     */
    protected function buildTransmitInput(array $input = []): array
    {
        switch (strtolower($this->input)) {
            default:
            case 'form':
                return $this->buildParams([
                    'form_params' => $input,
                ]);

                break;

            case 'json':
                return $this->buildParams([
                    'json' => $input,
                ]);

                break;
        }
    }

    /**
     * @param array $multipart
     * @return array
     */
    protected function buildMultipart(array $multipart): array
    {
        return $this->buildParams([
            'multipart' => $multipart,
        ]);
    }

    /**
     * @param object $data
     * @return string
     */
    protected function getStringResult(object $data): string
    {
        return (string)$data->getBody();
    }

    /**
     * @param object $data
     * @return object
     */
    protected function getJsonResult(object $data): object
    {
        return (object)json_decode($this->getStringResult($data), false);
    }

    /**
     * @param object $data
     * @return mixed
     */
    protected function getResult(object $data)
    {
        switch (strtolower($this->output)) {
            default:
            case 'json':
                return $this->getJsonResult($data);

                break;

            case 'string':
                return $this->getStringResult($data);

                break;
        }
    }

    /**
     * @param object $data
     * @return void
     */
    public function debug(object $data): void
    {
        $response = (string)$data->getBody();

        echo $response;
        exit();
    }

    /**
     * @param string $endpoint
     * @param array $params
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string $endpoint, array $params = [])
    {
        $response = $this->guzzle->get($endpoint, $this->buildQuery($params));

        return $this->getResult($response);
    }

    /**
     * @param string $endpoint
     * @param array $params
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post(string $endpoint, array $params = [])
    {
        $response = $this->guzzle->post($endpoint, $this->buildTransmitInput($params));

        return $this->getResult($response);
    }

    /**
     * @param string $endpoint
     * @param array $params
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function put(string $endpoint, array $params = [])
    {
        $response = $this->guzzle->put($endpoint, $this->buildTransmitInput($params));

        return $this->getResult($response);
    }

    /**
     * @param string $endpoint
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(string $endpoint)
    {
        $response = $this->guzzle->delete($endpoint);

        return $this->getResult($response);
    }

    /**
     * @param string $endpoint
     * @param array $uploads
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function upload(string $endpoint, array $uploads = []): string
    {
        $multipartUpload = $this->buildMultipart($uploads);

        $response = $this->guzzle->post($endpoint, $multipartUpload);

        return $this->getStringResult($response);
    }
}
