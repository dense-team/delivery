<?php
/**
 * User: Maros Jasan
 * Date: 3. 3. 2021
 * Time: 17:58
 */

namespace Dense\Delivery\Exception;

use GuzzleHttp\Exception\BadResponseException;

class Helpers
{
    /**
     * @param \GuzzleHttp\Exception\BadResponseException $e
     * @return string
     */
    public static function getExceptionResponse(BadResponseException $e): string
    {
        return (string)$e->getResponse()->getBody();
    }

    /**
     * @param \GuzzleHttp\Exception\BadResponseException $e
     * @return object
     */
    public static function getJsonExceptionResponse(BadResponseException $e): object
    {
        $response = self::getExceptionResponse($e);

        return json_decode($response);
    }

    /**
     * @param \GuzzleHttp\Exception\BadResponseException $e
     * @param int $offset
     * @return string
     */
    public static function getJsonReport(BadResponseException $e, int $offset = 0): string
    {
        $response = self::getJsonExceptionResponse($e);

        $exception = $response->reports[$offset];

        return $exception->message;
    }
}
