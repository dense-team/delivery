<?php
/**
 * User: Maros Jasan
 * Date: 8. 4. 2021
 * Time: 13:08
 */

namespace Dense\Delivery\Result;

use Illuminate\Support\Collection;

trait Hydrator
{
    /**
     * @param array $data
     * @param string $class
     * @return \Illuminate\Support\Collection
     */
    public function hydrateCollection(array $data, string $class): \Illuminate\Support\Collection
    {
        $collection = new Collection();

        foreach ($data as $item) {
            $result = $this->hydrate((array)$item, $class);

            $collection->push($result);
        }

        return $collection;
    }

    /**
     * @param array $data
     * @param string $class
     * @return object
     */
    public function hydrate(array $data, string $class)
    {
        $result = new $class();
        $result->hydrate($data);

        return $result;
    }
}
